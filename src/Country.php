<?php

class Country
{
  public function getCountryNameByCityName($city) {
    $french_cities = array('Bordeaux', 'Paris');

    if (in_array($city, $french_cities)) {
      $country = 'France';
    } else {
      $country = 'Unknow';
    }
    return $country;
  }
}