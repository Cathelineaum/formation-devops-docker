FROM ulsmith/alpine-apache-php7
ADD src /app/public
EXPOSE 80
RUN chown -R apache:apache /app
