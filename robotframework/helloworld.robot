*** Settings ***
Resource         resources/common.resource
Suite Setup      Open browser to home page
Suite Teardown   Close browser

*** Test Cases ***
Go to home page
    Wait Until Page Contains    Hello

Go to home page and send form
    ${EMAIL} =	Set Variable    test@gmail.com
    Input Text    id=inputEmail2    ${EMAIL}
    Click Element       //button[contains(text(),'SUBSCRIBE')]
